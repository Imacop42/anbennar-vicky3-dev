﻿STATE_EKYU_1 = {
    id = 270
    subsistence_building = "building_subsistence_pastures"
    provinces = { "x02FD3B" "x02FD84" "x19F81E" "x1F5555" "x1F55C5" "x22F6CA" "x2B9230" "x55453C" "x564A37" "x567637" "x625A03" "x78B789" "x86E3B5" "x9AA4B9" "xA810C9" "xA86DC9" "xB02050" "xB22178" "xC9478C" "xE32C31" "xE8124F" "xE98C0B" }
    traits = { state_trait_funashaweasin }
    city = "x02fd3b" #Random


    farm = "x78b789" #Random


    wood = "xb02050" #Random


    mine = "xa810c9" #Random


    arable_land = 1
    arable_resources = { bg_livestock_ranches }
    capped_resources = {
        bg_logging = 1
        bg_iron_mining = 3
        bg_lead_mining = 5
    }
}
STATE_EKYU_2 = {
    id = 271
    subsistence_building = "building_subsistence_pastures"
    provinces = { "x00DBA3" "x0D244F" "x12934C" "x1B6394" "x29A196" "x724807" "x7A4665" "x939325" "x99AF78" "x9B88F7" "xA45452" "xA5E729" "xA89044" "xADB202" "xB1895F" "xB45149" "xB8A52A" "xD05A2E" "xD312AC" "xD493AE" "xE44832" }
    traits = {}
    city = "xa45452" #Random


    farm = "xa5e729" #Random


    wood = "x99af78" #Random


    mine = "xb8a52a" #Random


    arable_land = 1
    arable_resources = { bg_livestock_ranches }
    capped_resources = {
        bg_logging = 1
        bg_iron_mining = 4
        bg_lead_mining = 7
    }
}
STATE_EKYU_3 = {
    id = 272
    subsistence_building = "building_subsistence_pastures"
    provinces = { "x0CC25B" "x0EE70B" "x107756" "x1080D0" "x147502" "x1782AF" "x1803D0" "x186D61" "x18C0CB" "x1F1368" "x21B59A" "x272C16" "x33807A" "x39D7C8" "x455C7F" "x4FDE31" "x50ADEE" "x528D86" "x7F8525" "x9068BD" "x933AD2" "x939F1B" "x9D360A" "x9D4993" "xA56FA1" "xAA1982" "xC71E45" "xCCAD63" "xCD6C45" "xCE1B54" "xDE52B4" "xF00110" "xF10B85" }
    traits = {}
    city = "x39d7c8" #Random


    farm = "x9d4993" #Random


    wood = "x528d86" #Random


    mine = "x1803d0" #Random


    arable_land = 1
    arable_resources = { bg_livestock_ranches }
    capped_resources = {
        bg_logging = 1
        bg_iron_mining = 5
        bg_lead_mining = 13
    }
}
STATE_EKYU_4 = {
    id = 273
    subsistence_building = "building_subsistence_pastures"
    provinces = { "x010000" "x0B75A6" "x8DE06D" "x9B5562" "xA77CD4" "xB08090" "xCC4A4F" }
    traits = {}
    city = "xcc4a4f" #Random


    farm = "x9b5562" #Random


    wood = "x0b75a6" #Random


    mine = "xa77cd4" #Random


    arable_land = 1
    arable_resources = { bg_livestock_ranches }
    capped_resources = {
        bg_logging = 1
        bg_iron_mining = 2
        bg_lead_mining = 3
        bg_coal_mining = 1
    }
}
