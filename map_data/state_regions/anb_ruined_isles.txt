﻿STATE_LASTSIGHT_ISLANDS = {
    #Bermuda x2
    id = 200
    subsistence_building = "building_subsistence_farms"
    provinces = { "x0D5C7E" "x0D6020" "x8D5EAD" "x8D624F" }
    traits = {}
    city = "x0d6020" #Ivrandir


    port = "x8d5ead" #Narawen


    farm = "x0d5c7e" #Taelarios


    arable_land = 8
    arable_resources = { bg_wheat_farms bg_livestock_ranches bg_banana_plantations bg_coffee_plantations bg_tobacco_plantations bg_sugar_plantations }
    capped_resources = {
        bg_fishing = 4
    }
    naval_exit_id = 3100
}

STATE_DANCING_SISTERS = {
    #West Indies 1/4
    id = 201
    subsistence_building = "building_subsistence_farms"
    provinces = { "x0E6C7E" "x4A347D" "x8E6A4F" "x8E6EAD" }
    traits = {}
    city = "x0e6c7e" #Isehris


    port = "x8e6ead" #The Twins


    farm = "x8e6a4f" #Arfil


    arable_land = 6
    arable_resources = { bg_rice_farms bg_livestock_ranches bg_banana_plantations bg_coffee_plantations bg_tobacco_plantations bg_sugar_plantations }
    capped_resources = {
        bg_fishing = 2
    }
    naval_exit_id = 3100
}

STATE_FOIRINT_ISLES = {
    #Bahamas 1/3
    id = 202
    subsistence_building = "building_subsistence_farms"
    provinces = { "x0e7020" "x1800f8" "x3cfcfc" }
    traits = {}
    city = "x0e7020" #Lohoolpcodds


    port = "x3cfcfc" #NEW PLACE


    farm = "x1800f8" #NEW PLACE


    arable_land = 2
    arable_resources = { bg_rice_farms bg_livestock_ranches bg_banana_plantations bg_coffee_plantations bg_tobacco_plantations bg_sugar_plantations }
    capped_resources = {
        bg_fishing = 2
    }
    naval_exit_id = 3101
}

STATE_SILSENSALD_ISLES = {
    #Puerto Rico
    id = 203
    subsistence_building = "building_subsistence_farms"
    provinces = { "x0D5820" "x3EC9FB" "x8D56AD" "x8D5A4F" }
    traits = {}
    city = "x8d5a4f" #Cas Colún


    port = "x3ec9fb" #East Brunvern


    farm = "x0d5820" #Silsensald


    wood = "x8d56ad" #Brunvern


    arable_land = 18
    arable_resources = { bg_rice_farms bg_livestock_ranches bg_banana_plantations bg_coffee_plantations bg_tobacco_plantations bg_sugar_plantations }
    capped_resources = {
        bg_logging = 2
        bg_fishing = 4
    }
    naval_exit_id = 3101
}

STATE_CAPE_OF_ENDRAL = {
    #Endralliande is Cuba x3 split acrosss 6 states, so each state is roughly half of Cuba
    id = 204
    subsistence_building = "building_subsistence_farms"
    provinces = { "x0F747E" "x220567" "x8F724F" "x8F76AD" "x99824F" "xD0FCE8" "xE4C6CA" }
    traits = {}
    city = "x8f76ad" #Criodhcel


    port = "x0f747e" #Celodirin


    farm = "x8f724f" #Sister's Coast


    #mine = "xd0fce8" #NEW PLACE
    wood = "x99824f" #Rhademele


    arable_land = 29
    arable_resources = { bg_rice_farms bg_livestock_ranches bg_banana_plantations bg_coffee_plantations bg_tobacco_plantations bg_sugar_plantations }
    capped_resources = {
        bg_logging = 8
        bg_fishing = 6
    #also 1 rare earths deposit
    }
    resource = {
        type = "bg_rubber"
        undiscovered_amount = 5
    }
    naval_exit_id = 3100
}

STATE_IBSEAN = {
    id = 205
    subsistence_building = "building_subsistence_farms"
    provinces = { "x0E647E" "x0E6820" "x0E747E" "x0F7820" "x8E66AD" "x967EAD" "x98794F" }
    traits = {}
    city = "x0e647e" #Tifur


    port = "x0e6820" #Fláruan


    farm = "x8e66ad" #Ibsthemar


    wood = "x0f7820" #Mórveren


    arable_land = 28
    arable_resources = { bg_rice_farms bg_livestock_ranches bg_banana_plantations bg_coffee_plantations bg_tobacco_plantations bg_sugar_plantations }
    capped_resources = {
        bg_logging = 9
        bg_fishing = 6
    }
    resource = {
        type = "bg_rubber"
        undiscovered_amount = 4
    }
    naval_exit_id = 3100
}

STATE_RUBENSHORE = {
    id = 206
    subsistence_building = "building_subsistence_farms"
    provinces = { "x167820" "x187020" "x18747E" "x7DC9C9" "x98724F" "xC7C959" }
    traits = {}
    city = "x18747e" #Rubenshore


    port = "x98724f" #Arbrínne


    farm = "x187020" #Scadhimeial


    wood = "x7dc9c9" #NEW PLACE (Rubenglade?)


    arable_land = 26
    arable_resources = { bg_maize_farms bg_livestock_ranches bg_cotton_plantations bg_tobacco_plantations bg_sugar_plantations }
    capped_resources = {
        bg_logging = 5
        bg_fishing = 6
    }
    naval_exit_id = 3101
}

STATE_JERCEL = {
    id = 207
    subsistence_building = "building_subsistence_farms"
    provinces = { "x177820" "x187820" "x5D892B" "x9876AD" "x987A4F" "xA930B6" "xC9C959" }
    traits = {}
    city = "x187820" #Jercel


    port = "x9876ad" #Brístentír


    farm = "xc9c959" #Aelthán


    mine = "xa930b6" #NEW PLACE


    wood = "x987a4f" #Aransilvar


    arable_land = 18
    arable_resources = { bg_rice_farms bg_livestock_ranches bg_cotton_plantations bg_tobacco_plantations bg_sugar_plantations }
    capped_resources = {
        bg_logging = 4
        bg_fishing = 6
        bg_iron_mining = 9
    }
    naval_exit_id = 3101
}

STATE_ENDRALVAR = {
    id = 208
    subsistence_building = "building_subsistence_farms"
    provinces = { "x240567" "x57BBEA" "x81AB81" "x834C0B" "x9A1755" "x9B4C0C" "x9B8B2F" "xE8C6CA" }
    traits = {}
    city = "x81ab81" #Upper Endralvar


    farm = "x9b8b2f" #Trásainé


    mine = "x9a1755" #NEW PLACE


    wood = "x9b4c0c" #Lower Endralvar


    arable_land = 31
    arable_resources = { bg_maize_farms bg_livestock_ranches bg_cotton_plantations bg_tobacco_plantations bg_sugar_plantations }
    capped_resources = {
        bg_logging = 8
        bg_iron_mining = 27
        bg_lead_mining = 24
    #also 1 uranium deposit and 1 rare earths
    }
}

STATE_MARBELOCH = {
    id = 209
    subsistence_building = "building_subsistence_farms"
    provinces = { "x187C7E" "x198020" "x8E724E" "x997EAD" "xC8C959" }
    traits = {}
    city = "x198020" #Cymcóst


    port = "x187c7e" #Márbeloch


    farm = "x997ead" #Endral's Reach


    arable_land = 21
    arable_resources = { bg_maize_farms bg_livestock_ranches bg_cotton_plantations bg_tobacco_plantations bg_sugar_plantations }
    capped_resources = {
        bg_logging = 5
        bg_fishing = 6
    }
    naval_exit_id = 3102
}

STATE_OBAITHAIL = {
    #Haiti + Santo Domingo
    id = 210
    subsistence_building = "building_subsistence_farms"
    provinces = { "x0F7C7E" "x108020" "x10847E" "x8F7A4F" "x8F7EAD" "x90824F" "xAD2E40" "xB52E40" }
    traits = {}
    city = "x8f7ead" #Soara


    port = "x8f7a4f" #Sámchí Móine


    farm = "x0f7c7e" #Orric Chéid


    #mine = "x10847e" #Náidreán
    wood = "x108020" #Dhainé


    arable_land = 53
    arable_resources = { bg_wheat_farms bg_livestock_ranches bg_banana_plantations bg_coffee_plantations bg_tobacco_plantations bg_sugar_plantations }
    capped_resources = {
        bg_logging = 10
        bg_fishing = 7
    #also 4 rare earths deposits
    }
    naval_exit_id = 3100
}

STATE_PIRENDRAL = {
    #Jamaica
    id = 211
    subsistence_building = "building_subsistence_farms"
    provinces = { "x1F5C7E" "x734EF0" "x9F5EAD" "xE6BE40" "xEE3F9A" }
    traits = {}
    city = "x9f5ead" #Qáthcel


    port = "x1f5c7e" #Irmathuan


    farm = "xe6be40" #Fadhin


    mine = "xee3f9a" #NEW PLACE


    wood = "x734ef0" #Thorimcóst


    arable_land = 18
    arable_resources = { bg_rice_farms bg_livestock_ranches bg_banana_plantations bg_coffee_plantations bg_tobacco_plantations bg_sugar_plantations }
    capped_resources = {
        bg_logging = 3
        bg_fishing = 5
        bg_iron_mining = 18
    #also 1 rare earths deposit
    }
    naval_exit_id = 3102
}

STATE_WANDERERS_GATE = {
    #Puerto Rico
    id = 212
    subsistence_building = "building_subsistence_farms"
    provinces = { "x206020" "x43EF0F" "x4A347C" "x9F5A4F" "xB726AD" "xC1B54F" }
    traits = {}
    city = "xb726ad" #Wanderer's Bay


    port = "x4a347c" #N Gate Islands


    farm = "x206020" #S Gate Islands


    wood = "x9f5a4f" #Traveller's Rest


    arable_land = 14
    arable_resources = { bg_rice_farms bg_livestock_ranches bg_banana_plantations bg_coffee_plantations bg_tobacco_plantations bg_sugar_plantations }
    capped_resources = {
        bg_logging = 2
        bg_fishing = 4
    }
    resource = {
        type = "bg_rubber"
        undiscovered_amount = 2
    }
    naval_exit_id = 3102
}

STATE_TORRISHEAH = {
    #Jamaica
    id = 213
    subsistence_building = "building_subsistence_farms"
    provinces = { "x227C7E" "x22E420" "x91210B" "x9F6A4F" "xA17A4F" "xA2E2AD" }
    traits = {}
    city = "xa2e2ad" #Sheáhin


    port = "x227c7e" #Sheáh Frigoria


    farm = "xa17a4f" #Gearúm Róg


    mine = "x91210b" #Márlas


    wood = "x22e420" #Árore


    arable_land = 18
    arable_resources = { bg_rice_farms bg_livestock_ranches bg_banana_plantations bg_coffee_plantations bg_tobacco_plantations bg_sugar_plantations }
    capped_resources = {
        bg_logging = 3
        bg_fishing = 5
    }
    resource = {
        type = "bg_rubber"
        undiscovered_amount = 4
    }
    naval_exit_id = 3103
}

STATE_FALILTCOST = {
    #Treeles Haiti-
    id = 214
    subsistence_building = "building_subsistence_farms"
    provinces = { "x05CACA" "x217820" "x49B677" "x4F37ED" "xA176AD" "xFDE2CE" }
    traits = {}
    city = "x05caca" #Fáliltverten


    port = "x49b677" #NEW PLACE


    farm = "xa176ad" #Lethpáiss


    mine = "x4f37ed" #NEW PLACE


    arable_land = 19
    arable_resources = { bg_maize_farms bg_livestock_ranches bg_cotton_plantations bg_tobacco_plantations bg_sugar_plantations }
    capped_resources = {
        bg_fishing = 7
        bg_iron_mining = 9
    #also 1 rare earths deposit
    }
    naval_exit_id = 3104
}

STATE_SANCTUARY_FIELDS = {
    #Haiti with less trees
    id = 215
    subsistence_building = "building_subsistence_farms"
    provinces = { "x217020" "x21747E" "x27447E" "xA1724F" }
    traits = {}
    city = "x21747e" #Ciallaire


    port = "xa1724f" #Ochchus Coast


    farm = "x217020" #Bacadh Trás


    wood = "x27447e" #Murmas


    arable_land = 23
    arable_resources = { bg_maize_farms bg_livestock_ranches bg_cotton_plantations bg_tobacco_plantations bg_sugar_plantations }
    capped_resources = {
        bg_logging = 3
        bg_fishing = 8
    }
    naval_exit_id = 3104
}

STATE_SILVERTENNAR = {
    #Santo Domingo-ish
    id = 216
    subsistence_building = "building_subsistence_farms"
    provinces = { "x206C7E" "x21657E" "x25447E" "x33EEDF" "xA06A4F" "xA16EAD" "xA7424F" "xB27942" }
    traits = {}
    city = "xa7424f" #Vertencóst


    port = "xa06a4f" #Cape of Revelations


    farm = "xa16ead" #Ineilainé


    mine = "xb27942" #NEW PLACE


    wood = "x206c7e" #Mórtrín


    arable_land = 21
    arable_resources = { bg_rice_farms bg_livestock_ranches bg_banana_plantations bg_tea_plantations bg_tobacco_plantations bg_sugar_plantations }
    capped_resources = {
        #Double the trees
        bg_logging = 14
        bg_fishing = 5
        bg_iron_mining = 18
    #also 1 rare earths deposit
    }
    resource = {
        type = "bg_rubber"
        undiscovered_amount = 7
    }
    naval_exit_id = 3103
}

STATE_VARIONAIL = {
    #Santo Domingo
    id = 217
    subsistence_building = "building_subsistence_farms"
    provinces = { "x20647E" "x206820" "x452A17" "x4A3820" "x4D37ED" "x8F4B4B" "xA0624F" "xA066AD" "xA0724F" "xA6424F" }
    traits = {}
    city = "x206820" #Eádohás


    port = "xa0624f" #Bronnáil


    farm = "xa066ad" #Vardihen


    mine = "x452a17" #NEW PLACE


    wood = "x20647e" #Dochéamur


    arable_land = 27
    arable_resources = { bg_rice_farms bg_livestock_ranches bg_banana_plantations bg_tea_plantations bg_tobacco_plantations bg_sugar_plantations }
    capped_resources = {
        #Double the trees
        bg_logging = 17
        bg_fishing = 5
        bg_iron_mining = 18
    #also 1 rare earths deposit
    }
    resource = {
        type = "bg_rubber"
        undiscovered_amount = 9
    }
    naval_exit_id = 3102
}

STATE_CORE_ISLANDS = {
    #Bahamas+
    id = 218
    subsistence_building = "building_subsistence_farms"
    provinces = { "x11CACA" "x22847E" "x228820" "x238C7E" "x239020" "x2ACBFD" "x3ACBFD" "xA2824F" "xA286AD" "xA38A4F" "xA38EAD" }
    traits = {}
    city = "xa286ad" #Ilzin Mykx


    port = "x228820" #Graxilzin / Núr Tef


    farm = "x238c7e" #Splashback


    mine = "xa38a4f" #Breccia Stacks


    wood = "xa2824f" #Graxarr / Úiscestir


    arable_land = 10
    arable_resources = { bg_rice_farms bg_livestock_ranches bg_banana_plantations bg_coffee_plantations bg_tobacco_plantations bg_sugar_plantations }
    capped_resources = {
        bg_logging = 4
        bg_fishing = 8
    }
    naval_exit_id = 3105
}

STATE_VERTEN = {
    #Haiti-
    id = 219
    subsistence_building = "building_subsistence_farms"
    provinces = { "x16CAFC" "x23947E" "x735670" "xA3924F" }
    traits = {}
    city = "xa3924f" #North Verten


    port = "x23947e" #West Verten


    farm = "x735670" #East Verten


    wood = "x16cafc" #NEW PLACE (Central South Verten)


    arable_land = 24
    arable_resources = { bg_rice_farms bg_livestock_ranches bg_banana_plantations bg_coffee_plantations bg_tobacco_plantations bg_sugar_plantations }
    capped_resources = {
        bg_logging = 4
        bg_fishing = 4
    }
    resource = {
        type = "bg_rubber"
        undiscovered_amount = 1
    }
    naval_exit_id = 3106    #Reaper's Coast
}

STATE_ISLES_OF_PLENTY = {
    #West Indies
    id = 220
    subsistence_building = "building_subsistence_farms"
    provinces = { "x20FCFC" "x249820" "x249C7E" "x24E820" "x26347E" "x3DC9C9" "xA396AD" "xA4224F" "xA49A4F" "xA49EAD" "xA636AD" "xA63A4F" }
    traits = {}
    city = "x249820" #New Calas


    port = "xa636ad" #New Ainway


    farm = "xa63a4f" #Crownisle


    wood = "x24e820" #Endless Sun


    arable_land = 24
    arable_resources = { bg_rice_farms bg_livestock_ranches bg_banana_plantations bg_coffee_plantations bg_tobacco_plantations bg_sugar_plantations }
    capped_resources = {
        bg_fishing = 8
    }
    naval_exit_id = 3107    #Sea of Respite
}

STATE_TRICKSTER_ISLE = {
    #Average between Santo Domingo and Jamaica
    id = 221
    subsistence_building = "building_subsistence_farms"
    provinces = { "x24247E" "x252C7E" "x25F020" "xA526AD" "xA52A4F" "xA52EAD" "xF5E251" }
    traits = {}
    city = "x24247e" #Hemnisedlank


    port = "x25f020" #Brooskneddlag


    farm = "xa52ead" #North Reprieve


    mine = "xa52a4f" #Soblickdoorsd


    wood = "xa526ad" #Plotslickslump


    arable_land = 20
    arable_resources = { bg_rice_farms bg_livestock_ranches bg_banana_plantations bg_coffee_plantations bg_tobacco_plantations bg_sugar_plantations }
    capped_resources = {
        bg_logging = 7
        bg_fishing = 6
        bg_iron_mining = 18
    }
    resource = {
        type = "bg_rubber"
        undiscovered_amount = 4
    }
    naval_exit_id = 3105
}

STATE_ISLE_OF_TRANSITION = {
    #Bahamas+
    id = 222
    subsistence_building = "building_subsistence_farms"
    provinces = { "x25F820" "x263C7E" "x26A420" "x26F720" "x7A0010" "xA5324F" "xA63EAD" "xF2544E" }
    traits = {}
    city = "x263c7e" #Chesh


    port = "x26f720" #Reaper's Due


    farm = "xa5324f" #Ruined Stacks


    wood = "x26a420" #Isle of Death


    arable_land = 8
    arable_resources = { bg_rice_farms bg_livestock_ranches bg_banana_plantations bg_coffee_plantations bg_tobacco_plantations bg_sugar_plantations }
    capped_resources = {
        bg_logging = 2
        bg_fishing = 5
    }
    naval_exit_id = 3106
}

STATE_SAAMIRSES = {
    #Puerto Rico
    id = 223
    subsistence_building = "building_subsistence_farms"
    provinces = { "x1F547E" "x1F5820" "x33AE7D" "x85B7FE" "x9F56AD" "xCFB481" }
    traits = {}
    city = "x1f547e" #Saamiribar


    port = "x1f5820" #Uturakal


    farm = "x9f56ad" #Ulmišore


    arable_land = 18
    arable_resources = { bg_rice_farms bg_livestock_ranches bg_banana_plantations bg_coffee_plantations bg_tobacco_plantations bg_sugar_plantations }
    capped_resources = {
        bg_logging = 2
        bg_fishing = 4
    }
    naval_exit_id = 3106
}

STATE_GREAT_TUSSOCK = {
    #Treeless Jamaica
    id = 224
    subsistence_building = "building_subsistence_farms"
    provinces = { "x27DFFD" "x768250" "x9A8EAD" }
    traits = {}
    city = "x9a8ead" #Pointy End


    port = "x27dffd" #NEW PLACE


    farm = "x768250" #Fat End


    arable_land = 18
    arable_resources = { bg_rice_farms bg_livestock_ranches bg_banana_plantations bg_coffee_plantations bg_tobacco_plantations bg_sugar_plantations }
    capped_resources = {
        bg_fishing = 5
    }
    naval_exit_id = 3108    #Trollsbay
}
