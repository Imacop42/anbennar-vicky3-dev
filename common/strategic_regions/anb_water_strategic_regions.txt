﻿
region_dameshead = {
	states = { STATE_DAMESHEAD }
}

region_damesneck = {
	states = { STATE_DAMESNECK }
}

region_west_diven = {
	states = { STATE_WEST_DIVEN }
}

region_divengate = {
	states = { STATE_DIVENGATE }
}

region_central_diven = {
	states = { STATE_CENTRAL_DIVEN }
}

region_tefkora_pass = {
	states = { STATE_TEFKORA_PASS }
}

region_east_diven = {
	states = { STATE_EAST_DIVEN }
}

region_bay_of_wines = {
	states = { STATE_BAY_OF_WINES }
}

region_coast_of_venail = {
	states = { STATE_COAST_OF_VENAIL }
}

region_westcoast = {
	states = { STATE_WESTCOAST }
}

region_giants_grave_sea = {
	states = { STATE_GIANTS_GRAVE_SEA }
}

region_frozen_pass = {
	states = { STATE_FROZEN_PASS }
}


region_north_reavers_sea = {
	states = { STATE_NORTH_REAVERS_SEA }
}

region_whaleroad = {
	states = { STATE_WHALEROAD }
}

region_lonely_isle_sea = {
	states = { STATE_LONELY_ISLE_SEA }
}

region_dragons_breath_sea = {
	states = { STATE_DRAGONS_BREATH_SEA }
}

region_lonely_far_lane_3 = {
	states = { STATE_LONELY_FAR_LANE_3 }
}

region_far_isle_coast = {
	states = { STATE_FAR_ISLE_COAST }
}

region_far_isle_coast_lane_1 = {
	states = { STATE_FAR_ISLE_COAST_LANE_1 }
}

region_far_isle_coast_lane_2 = {
	states = { STATE_FAR_ISLE_COAST_LANE_2 }
}

region_devils_lane = {
	states = { STATE_DEVILS_LANE }
}

region_edillions_relief_lane = {
	states = { STATE_EDILLIONS_RELIEF_LANE }
}

region_uelos_lament_5 = {
	states = { STATE_UELOS_LAMENT_5 }
}

region_banished_sea = {
	states = { STATE_BANISHED_SEA }
}

region_silsensald_sea = {
	states = { STATE_SILSENSALD_SEA }
}

region_calamity_pass = {
	states = { STATE_CALAMITY_PASS }
}

region_endrals_way = {
	states = { STATE_ENDRALS_WAY }
}

region_ochchus_sea = {
	states = { STATE_OCHCHUS_SEA }
}

region_reapers_coast = {
	states = { STATE_REAPERS_COAST }
}

region_sea_of_respite = {
	states = { STATE_SEA_OF_RESPITE }
}

region_trollsbay_coast = {
	states = { STATE_TROLLSBAY_COAST }
}