﻿state_trait_dalairey_wastes = {	
    icon = "gfx/interface/icons/state_trait_icons/malaria.dds"
	
	required_techs_for_colonization = { "quinine" }	#TODO
	disabling_technologies = { "malaria_prevention" }
	
	modifier = {
		state_non_homeland_colony_growth_speed_mult = -0.9
		state_non_homeland_mortality_mult = 0.75
	}
}

state_trait_salahad_desert = {
	icon = "gfx/interface/icons/state_trait_icons/dry_climate.dds"
	 
	modifier = {
        building_group_bg_agriculture_throughput_mult = -0.2
		state_construction_mult = -0.25
		state_infrastructure_mult = -0.25
    }
}