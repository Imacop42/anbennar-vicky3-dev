﻿COUNTRIES = {
	c:A01 = {
		effect_starting_technology_tier_2_tech = yes
		
		add_taxed_goods = g:grain
		
		activate_law = law_type:law_monarchy
		activate_law = law_type:law_wealth_voting	#compromise by baronites and conservatives
		activate_law = law_type:law_cultural_exclusion
		activate_law = law_type:law_freedom_of_conscience
		activate_law = law_type:law_elected_bureaucrats
		activate_law = law_type:law_national_militia	#anbennar's first task is to get this reform going
		activate_law = law_type:law_national_guard

		activate_law = law_type:law_interventionism
		activate_law = law_type:law_free_trade	#enforced by powers so Anbennar keeps open
		activate_law = law_type:law_per_capita_based_taxation
		activate_law = law_type:law_no_colonial_affairs
		activate_law = law_type:law_local_police
		activate_law = law_type:law_charitable_health_system
		activate_law = law_type:law_private_schools
		activate_law = law_type:law_artifice_encouraged

		activate_law = law_type:law_censorship
		activate_law = law_type:law_serfdom_banned
		activate_law = law_type:law_child_labor_allowed
		activate_law = law_type:law_women_own_property
		activate_law = law_type:law_slavery_banned
		
		activate_law = law_type:law_all_races_allowed
		activate_law = law_type:law_dark_arts_banned
		activate_law = law_type:law_amoral_artifice_banned
		activate_law = law_type:law_artifice_encouraged

		#All conspirators of first BPC
		ig:ig_intelligentsia = {
			add_ruling_interest_group = yes
		}
		ig:ig_devout = {
			add_ruling_interest_group = yes
			set_interest_group_name = ig_ravelian_church
			remove_ideology = ideology_moralist
			add_ideology = ideology_ravelian_moralist
			remove_ideology = ideology_pious
			add_ideology = ideology_scholarly
		}
		ig:ig_landowners = {
			add_ruling_interest_group = yes
			set_interest_group_name = ig_baronites
			remove_ideology = ideology_paternalistic
			add_ideology = ideology_baronites_paternalistic
		}
		ig:ig_armed_forces = {
			add_ruling_interest_group = yes
		}
	}
}