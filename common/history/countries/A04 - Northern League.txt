﻿COUNTRIES = {
	c:A04 = {
		effect_starting_technology_tier_1_tech = yes
		
		activate_law = law_type:law_parliamentary_republic
		activate_law = law_type:law_oligarchy
		activate_law = law_type:law_national_supremacy
		activate_law = law_type:law_freedom_of_conscience
		activate_law = law_type:law_appointed_bureaucrats		
		activate_law = law_type:law_professional_army
		# No home affairs
		activate_law = law_type:law_laissez_faire
		activate_law = law_type:law_free_trade
		activate_law = law_type:law_per_capita_based_taxation
		activate_law = law_type:law_colonial_exploitation
		activate_law = law_type:law_no_police
		activate_law = law_type:law_private_schools
		# No healthcare
		
		activate_law = law_type:law_censorship
		activate_law = law_type:law_serfdom_banned
		activate_law = law_type:law_child_labor_allowed
		activate_law = law_type:law_women_own_property
		activate_law = law_type:law_poor_laws
		activate_law = law_type:law_slavery_banned
		
		activate_law = law_type:law_non_monstrous_only
		activate_law = law_type:law_dark_arts_banned
		activate_law = law_type:law_amoral_artifice_banned
		activate_law = law_type:law_artifice_encouraged

		ig:ig_armed_forces = {
			set_interest_group_name = ig_old_guard
			add_ruling_interest_group = yes
		}
		ig:ig_industrialists = {
			set_interest_group_name = ig_trade_barons
			add_ruling_interest_group = yes
		}
		ig:ig_intelligentsia = {
			set_interest_group_name = ig_coopertatists
			add_ruling_interest_group = yes
		}
		ig:ig_landowners = { 
			set_ig_suppression = yes
			remove_ideology = ideology_paternalistic
			add_ideology = ideology_republican_paternalistic
		}		
		ig:ig_devout = {
			set_interest_group_name = ig_ravelian_church
			remove_ideology = ideology_moralist
			add_ideology = ideology_ravelian_moralist
			remove_ideology = ideology_pious
			add_ideology = ideology_scholarly
		}

		set_institution_investment_level = {
			institution = institution_colonial_affairs
			level = 2
		}
		
		set_institution_investment_level = {
			institution = institution_schools
			level = 2
		}
		
		add_journal_entry = { type = je_urbanization_of_gerudia }
		add_journal_entry = { type = je_urbanization_of_the_alen }
		add_journal_entry = { type = je_urbanization_of_the_reach }
	}
}