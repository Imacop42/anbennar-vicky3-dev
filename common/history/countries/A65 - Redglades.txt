﻿COUNTRIES = {
	c:A65 = {
		effect_starting_technology_tier_2_tech = yes
		
		activate_law = law_type:law_presidential_republic
		activate_law = law_type:law_oligarchy
		activate_law = law_type:law_national_supremacy
		activate_law = law_type:law_state_religion
		activate_law = law_type:law_hereditary_bureaucrats
		activate_law = law_type:law_peasant_levies
		activate_law = law_type:law_agrarianism
		activate_law = law_type:law_protectionism
		activate_law = law_type:law_per_capita_based_taxation
		activate_law = law_type:law_no_colonial_affairs
		activate_law = law_type:law_private_schools
		activate_law = law_type:law_charitable_health_system
		activate_law = law_type:law_censorship
		activate_law = law_type:law_serfdom_banned
		activate_law = law_type:law_child_labor_allowed
		activate_law = law_type:law_womens_suffrage
		activate_law = law_type:law_slavery_banned
		activate_law = law_type:law_migration_controls

		activate_law = law_type:law_same_race_and_humans
		activate_law = law_type:law_dark_arts_banned
		activate_law = law_type:law_amoral_artifice_banned
		activate_law = law_type:law_traditional_magic_only

		ig:ig_landowners = {
			add_ruling_interest_group = yes
			remove_ideology = ideology_paternalistic
			add_ideology = ideology_elven_havenist
		}
		ig:ig_devout = {
			set_interest_group_name = ig_adeanic_temples
			remove_ideology = ideology_moralist
			add_ideology = ideology_adeanic_moralist
		}
	}
}