﻿POPS = {
	s:STATE_NOGRUD = {
		region_state:C03 = {
			create_pop = {
				culture = ozgar_orc
				size = 100
			}
			create_pop = {
				culture = soruinic
				size = 100
			}
			create_pop = {
				culture = effe_i
				size = 100
			}
		}
	}
	s:STATE_KARLUR_DARAKH = {
		region_state:C03 = {
			create_pop = {
				culture = ozgar_orc
				size = 100
			}
			create_pop = {
				culture = effe_i
				size = 100
			}
		}
	}
	s:STATE_YARUHOL = {
		region_state:C03 = {
			create_pop = {
				culture = ozgar_orc
				size = 100
			}
			create_pop = {
				culture = soruinic
				size = 100
			}
		}
	}
	s:STATE_FASHTUG = {
		region_state:C03 = {
			create_pop = {
				culture = ozgar_orc
				size = 100
			}
			create_pop = {
				culture = soruinic
				size = 100
			}
		}
	}
	s:STATE_OZGAR = {
		region_state:C03 = {
			create_pop = {
				culture = ozgar_orc
				size = 100
			}
			create_pop = {
				culture = soruinic
				size = 100
			}
		}
	}
	s:STATE_BRAMMYAR = {
		region_state:C03 = {
			create_pop = {
				culture = ozgar_orc
				size = 100
			}
			create_pop = {
				culture = soruinic
				size = 100
			}
		}
	}
	s:STATE_JIBIRAEN = {
		region_state:C04 = {
			create_pop = {
				culture = soruinic
				size = 100
			}
			create_pop = {
				culture = ozgar_orc
				size = 100
			}
		}
	}
	s:STATE_MARUKHAN = {
		region_state:C03 = {
			create_pop = {
				culture = ozgar_orc
				size = 100
			}
			create_pop = {
				culture = parura
				size = 100
			}
			create_pop = {
				culture = leechman
				size = 100
			}
		}
	}
	s:STATE_THE_MIDDANS = {
		region_state:C06 = {
			create_pop = {
				culture = swampman
				size = 100
			}
			create_pop = {
				culture = leechman
				size = 100
			}
			create_pop = {
				culture = parura
				size = 100
			}
		}
		region_state:C03 = {
			create_pop = {
				culture = ozgar_orc
				size = 100
			}
			create_pop = {
				culture = leechman
				size = 100
			}
		}
		region_state:A08 = {
			create_pop = {
				culture = tefori
				size = 100
			}
			create_pop = {
				culture = parura
				size = 100
			}
			create_pop = {
				culture = leechman
				size = 100
			}
		}
	}
	s:STATE_SCREAMING_JUNGLE = {
		region_state:C12 = {
			create_pop = {
				culture = parura
				size = 100
			}
			create_pop = {
				culture = leechman
				size = 100
			}
		}
		region_state:C13 = {
			create_pop = {
				culture = parura
				size = 100
			}
			create_pop = {
				culture = leechman
				size = 100
			}
		}
	}
	s:STATE_WESTERN_EFFELAI = {
		region_state:C14 = {
			create_pop = {
				culture = seedthrall
				size = 100
			}
			create_pop = {
				culture = effe_i
				size = 100
			}
		}
		region_state:C15 = {
			create_pop = {
				culture = effe_i
				size = 100
			}
			create_pop = {
				culture = lai_i
				size = 100
			}
		}
	}
	s:STATE_MUSHROOM_FOREST = {
		region_state:C16 = {
			create_pop = {
				culture = lai_i
				size = 100
			}
		}
	}
	s:STATE_CENTRAL_EFFELAI = {
		region_state:C14 = {
			create_pop = {
				culture = seedthrall
				size = 100
			}
			create_pop = {
				culture = parura
				size = 100
			}
		}
		region_state:C06 = {
			create_pop = {
				culture = swampman
				size = 100
			}
			create_pop = {
				culture = parura
				size = 100
			}
			create_pop = {
				culture = seedthrall
				size = 100
			}
		}
	}
	s:STATE_EASTERN_EFFELAI = {
		region_state:C14 = {
			create_pop = {
				culture = seedthrall
				size = 100
			}
			create_pop = {
				culture = effe_i
				size = 100
			}
			create_pop = {
				culture = lai_i
				size = 100
			}
		}
	}
	s:STATE_SORFEN = {
		region_state:C06 = {
			create_pop = {
				culture = swampman
				size = 100
			}
			create_pop = {
				culture = leechman
				size = 100
			}
			create_pop = {
				culture = parura
				size = 100
			}
		}
	}
	s:STATE_SEINAINE = {
		region_state:C05 = {
			create_pop = {
				culture = arbarani
				size = 100
			}
			create_pop = {
				culture = leechman
				size = 100
			}
			create_pop = {
				culture = parura
				size = 100
			}
		}
		region_state:C06 = {
			create_pop = {
				culture = swampman
				size = 100
			}
			create_pop = {
				culture = parura
				size = 100
			}
		}
	}
	s:STATE_DALAINE = {
		region_state:C05 = {
			create_pop = {
				culture = arbarani
				size = 100
			}
			create_pop = {
				culture = thavo_i
				size = 100
			}
		}
	}
	s:STATE_DEEPSONG = {
		region_state:C17 = {
			create_pop = {
				culture = thavo_i
				size = 100
			}
			create_pop = {
				culture = seedthrall
				size = 100
			}
		}
		region_state:C14 = {
			create_pop = {
				culture = seedthrall
				size = 100
			}
			create_pop = {
				culture = thavo_i
				size = 100
			}
		}
	}
	s:STATE_THALASARAN = {
		region_state:C08 = {
			create_pop = {
				culture = vernman
				size = 100
			}
			create_pop = {
				culture = roilsardi
				size = 100
			}
			create_pop = {
				culture = thavo_i
				size = 100
			}
		}
		region_state:A03 = {
			create_pop = {
				culture = lorentish
				size = 100
			}
			create_pop = {
				culture = vernman
				size = 100
			}
			create_pop = {
				culture = thavo_i
				size = 100
			}
		}
	}
	s:STATE_KIOHALEN = {
		region_state:C07 = {
			create_pop = {
				culture = kioha_harpy
				size = 100
			}
			create_pop = {
				culture = thavo_i
				size = 100
			}
			create_pop = {
				culture = vernman
				size = 100
			}
		}
	}
	s:STATE_KIINDTIR = {
		region_state:C07 = {
			create_pop = {
				culture = kioha_harpy
				size = 100
			}
			create_pop = {
				culture = thavo_i
				size = 100
			}
			create_pop = {
				culture = vernman
				size = 100
			}
		}
	}
	s:STATE_DAZINKOST = {
		region_state:C01 = {
			create_pop = {
				culture = dawn_elf
				size = 100
			}
			create_pop = {
				culture = rezankandish
				size = 100
			}
			create_pop = {
				culture = oono_i
				size = 100
			}
		}
	}
	s:STATE_REZANOAN = {
		region_state:C01 = {
			create_pop = {
				culture = dawn_elf
				size = 100
			}
			create_pop = {
				culture = rezankandish
				size = 100
			}
			create_pop = {
				culture = oono_i
				size = 100
			}
		}
	}
	s:STATE_BROAN_KEIR = {
		region_state:C01 = {
			create_pop = {
				culture = dawn_elf
				size = 100
			}
			create_pop = {
				culture = rezankandish
				size = 100
			}
			create_pop = {
				culture = oono_i
				size = 100
			}
		}
	}
	s:STATE_NUREL = {
		region_state:C01 = {
			create_pop = {
				culture = dawn_elf
				size = 100
			}
			create_pop = {
				culture = rezankandish
				size = 100
			}
			create_pop = {
				culture = oono_i
				size = 100
			}
		}
	}
	s:STATE_ARENEL = {
		region_state:C01 = {
			create_pop = {
				culture = dawn_elf
				size = 100
			}
			create_pop = {
				culture = rezankandish
				size = 100
			}
			create_pop = {
				culture = oono_i
				size = 100
			}
			create_pop = {
				culture = lai_i
				size = 100
			}
		}
	}
	s:STATE_NUR_ELIZNA = {
		region_state:C01 = {
			create_pop = {
				culture = dawn_elf
				size = 100
			}
			create_pop = {
				culture = rezankandish
				size = 100
			}
		}
	}
	s:STATE_NEWSHORE = {
		region_state:C10 = {
			create_pop = {
				culture = malateli
				size = 100
			}
		}
	}
	s:STATE_TIMBERNECK = {
		region_state:C10 = {
			create_pop = {
				culture = malateli
				size = 100
			}
		}
	}
	s:STATE_VRENDIN = {
		region_state:C10 = {
			create_pop = {
				culture = malateli
				size = 100
			}
			create_pop = {
				culture = thekvrystana
				size = 100
			}
		}
	}
	s:STATE_AMANTAIL = {
		region_state:A01 = {
			create_pop = {
				culture = pearlsedger
				size = 100
			}
		}
	}
	s:STATE_QUIET_ISLE = {
		region_state:A01 = {
			create_pop = {
				culture = pearlsedger
				size = 100
			}
		}
	}
	s:STATE_LAIPOINT_ARCHIPELAGO = {
		region_state:A01 = {
			create_pop = {
				culture = pearlsedger
				size = 100
			}
		}
		region_state:A08 = {
			create_pop = {
				culture = tefori
				size = 100
			}
		}
	}
	s:STATE_WEST_TURTLEBACK = {
		region_state:C09 = {
			create_pop = {
				culture = busilari
				size = 100
			}
			create_pop = {
				culture = pearlsedger
				size = 100
			}
		}
	}
	s:STATE_EAST_TURTLEBACK = {
		region_state:C09 = {
			create_pop = {
				culture = busilari
				size = 100
			}
			create_pop = {
				culture = pearlsedger
				size = 100
			}
		}
	}
}