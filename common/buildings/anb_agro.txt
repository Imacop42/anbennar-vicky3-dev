﻿############# FARMING & RANCHING

building_serpentbloom_farm = {
	building_group = bg_serpentbloom_farms
	
	texture = "gfx/interface/icons/building_icons/rye_farm.dds"

	city_type = farm
	levels_per_mesh = 5

	unlocking_technologies = {
		enclosure
	}

	production_method_groups = {
		pmg_base_building_serpentbloom_farm
		pmg_secondary_building_serpentbloom_farm
		pmg_harvesting_process_building_serpentbloom_farm
		pmg_ownership_land_building_serpentbloom_farm
	}

	required_construction = construction_cost_low
	
	terrain_manipulator = farmland_rye
}