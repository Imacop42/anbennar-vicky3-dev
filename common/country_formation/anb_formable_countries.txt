﻿B87 = { #Eordand
	use_culture_states = yes

	required_states_fraction = 0.75
	
	ai_will_do = { always = yes }

	possible = {
		any_country = {
			OR = {
				country_has_primary_culture = cu:peitar
				country_has_primary_culture = cu:selphereg
				country_has_primary_culture = cu:caamas
				country_has_primary_culture = cu:tuathak
				country_has_primary_culture = cu:snecboth
				country_has_primary_culture = cu:fograc
			}			
			has_technology_researched = pan-nationalism
		}
	}
}